import { makeObservable, observable, action } from 'mobx'
import Connector from '@models/Connector'

import CycleStore from '@stores/CycleStore'
import PresetStore from '@stores/PresetStore'
import LocalSceneStore from '@stores/LocalSceneStore'

import { CycleEnum } from '@models/CycleEnum'

import JsonConnectors from '@assets/json/connectors.json'

/**
 * @classdesc Stores all connectors
 * @memberof stores
 */
class ConnectorStore {
  availableConnectors = new Set(Connector.fromArray(JsonConnectors))

  /** @property {Map<string, object>} selectedConnectors - All selected connectors hashed by pinID */
  selectedConnectors = new Map()

  /** @property {string} currentConnectorId - The ID of the current connector */
  currentConnectorId = ''

  constructor (presetStore, cycleStore, localSceneStore) {
    if (presetStore instanceof PresetStore) {
      this.presetStore = presetStore
    } else {
      throw new TypeError('PresetStore is required')
    }

    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    if (localSceneStore instanceof LocalSceneStore) {
      this.localSceneStore = localSceneStore
    } else {
      throw new TypeError('LocalSceneStore is required')
    }

    makeObservable(this, {
      selectedConnectors: observable,
      currentConnectorId: observable,

      setSelectedConnectors: action,
      setCurrentConnectorId: action
    })
  }

  /**
   * Gets the usb connectors from all available connectors
   * @returns {Object} usbConnectors - Array of all available usb connectors
   */
  get usbConnectors () {
    return [...this.availableConnectors].filter(c => ['USB'].includes(c.type))
  }

  /**
   * Gets the audio connectors from all available connectors
   * @returns {Object} audioConnectors - Array of all available audio connectors
   */
  get audioConnectors () {
    return [...this.availableConnectors].filter(c => ['AUDIO'].includes(c.type))
  }

  /**
   * Gets the hdmi connectors from all available connectors
   * @returns {Object} hdmiConnectors - Array of all available hdmi connectors
   */
  get hdmiConnectors () {
    return [...this.availableConnectors].filter(c => ['HDMI'].includes(c.type))
  }

  /**
   * Gets the video connectors from all available connectors
   * @returns {Object} videoConnectors - Array of all available video connectors
   */
  get videoConnectors () {
    return [...this.availableConnectors].filter(c => c.media.includes('video'))
  }

  /**
   * Checks if the user can test a connector
   * @returns {Boolean} - Flags true if the user can test the connector
   */
  get canTestConnector () {
    const currentPinId = this.localSceneStore.currentPin.id
    return (this.cycleStore.currentCycle === CycleEnum.TESTING || this.localSceneStore.isCheckedPin(currentPinId)) &&
    this.selectedConnectors.has(currentPinId)
  }

  /**
   * Updates the selected connections map according to user selection
   * @param {string} pinId - The ID of the connector
   * @param {object} connector - The connector object
   */
  updateSelectedConnectors (pinId, connector) {
    const isSelectedConnector = this.selectedConnectors.get(pinId)

    if (isSelectedConnector) {
      if (this.selectedConnectors.get(pinId) === connector.id) {
        this.selectedConnectors.delete(pinId)
      } else {
        this.selectedConnectors.set(pinId, connector.id)
      }
    } else {
      this.selectedConnectors.set(pinId, connector.id)
    }
  }

  /**
   * Groups the connectors according to their type
   * @param {string} type - The type of the connector
   * @returns {Object} connectorsGroup - Array of the connectors of same type
   */
  getConnectorsByType (type) {
    let connectorsGroup = []

    switch (type) {
      case ('USB'):
        connectorsGroup = this.usbConnectors
        break
      case ('AUDIO'):
        connectorsGroup = this.audioConnectors
        break
      case ('HDMI'):
        connectorsGroup = this.hdmiConnectors
        break
      default:
        connectorsGroup = []
        break
    }

    return connectorsGroup
  }

  /**
   * Checks if a connector is saved or not
   * @param {string} connectorId - The id of the connector
   * @returns {Boolean} inputConnectors - Returns true if the connector is saved
   */
  isConnectorSaved (connectorId) {
    let isSaved = false
    for (const id of this.selectedConnectors.values()) {
      if (id === connectorId) { isSaved = true }
    }
    return isSaved
  }

  /**
   * Checks if a connector is disabled
   * @param {object} connector - The connector to check
   * @returns {Boolean} inputConnectors - Returns true if the connector is disabled
   */
  isDisabledConnector (connector) {
    return (Array.from(this.selectedConnectors.values()).includes(connector.id) && connector.id !== this.currentConnectorId) ||
    !connector.media.includes(this.localSceneStore.currentPin.media)
  }

  /**
   * Sets the current connector ID
   * @param {string} connectorId - The id of the connector
  */
  setCurrentConnectorId (connectorId) {
    this.currentConnectorId = connectorId
  }

  /**
  * Sets the selected connectors
  * @param {string} pinId - The Id of the connector
  * @param {object} connector - The connector to select
  */
  setSelectedConnectors (pinId, connector) {
    this.selectedConnectors.set(pinId, connector)
  }
}

export default ConnectorStore
