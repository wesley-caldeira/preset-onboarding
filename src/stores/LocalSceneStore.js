import { makeObservable, observable, action, computed } from 'mobx'

import PresetStore from '@stores/PresetStore'
import CycleStore from '@stores/CycleStore'

import LocalScene from '@models/LocalScene'

import JsonScenes from '@assets/json/scenes.json'

/**
 * @classdesc Stores all scenes
 * @memberof stores
 */
class LocalSceneStore {
  availablePins = LocalScene.fromArray(JsonScenes)

  /** @property {Object} activePin - The active pin */
  activePin = null

  /** @property {Map<string, object>} checkedPins - All checked pins hashed by pinID */
  checkedPins = new Map()

  /** @property {Object} currentPin - The pin that the user is connecting/configuring */
  currentPin = null

  constructor (presetStore, cycleStore) {
    if (presetStore instanceof PresetStore) {
      this.presetStore = presetStore
    } else {
      throw new TypeError('PresetStore is required')
    }

    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    makeObservable(this, {
      availablePins: observable,
      activePin: observable,
      checkedPins: observable,
      currentPin: observable,

      setCurrentPin: action,
      setActivePin: action,
      setCheckedPin: action,
      isTestingVideoEntry: computed,
      isTestingAudioEntry: computed

    })
  }

  /**
   * Checks if a pin is active
   * @param {string} pinId - The id of the pin
   * @returns {Boolean} - Flags true if the pin is active
   */
  isActivePin (pinId) {
    return this.activePin?.id === pinId
  }

  /**
   * Checks if a pin is checked
   * @param {string} pinId - The id of the pin
   * @returns {Boolean} - Flags true if the pin is checked
   */
  isCheckedPin (pinId) {
    return this.checkedPins.has(pinId)
  }

  /**
   * Checks if a tested pin is of media video
   * @returns {Boolean} - Flags true if the tested pin is of type video
   */
  get isTestingVideoEntry () {
    return this.currentPin.media === 'video'
  }

  /**
   * Checks if a tested pin is of media audio
   * @returns {Boolean} - Flags true if the tested pin is of type audio
   */
  get isTestingAudioEntry () {
    return this.currentPin.media === 'audio'
  }

  /**
  * Sets the active pin
  * @param {Object} pin - The connector object
  */
  setActivePin (pin) {
    this.activePin = pin
  }

  /**
  * Sets the checked pin
  * @param {string} pinId - The Id of the connector
  * @param {Object} pin - The connector object
  */
  setCheckedPin (pinId, pin) {
    this.activePin = null
    this.checkedPins.set(pinId, pin)
  }

  /**
  * Sets the current pin
  * @param {Object} pin - The connector object
  */
  setCurrentPin (pin) {
    this.currentPin = pin
  }
}

export default LocalSceneStore
