import Preset from '@models/Preset'
import { makeObservable, observable, action } from 'mobx'

import CycleStore from '@stores/CycleStore'

/**
 * @classdesc Stores all presets
 * @memberof stores
 */
class PresetStore {
  preset = null

  constructor (cycleStore) {
    if (cycleStore instanceof CycleStore) {
      this.cycleStore = cycleStore
    } else {
      throw new TypeError('CycleStore is required')
    }

    makeObservable(this, {
      preset: observable,
      setPreset: action
    })
  }

  /** @property {string} - The instructions for the connection to configure */
  get connectionInstructions () {
    return this.preset.connectionInstructions
  }

  /** @property {Object} - The list of devices required for the preset */
  get deviceList () {
    return this.preset?.deviceList
  }

  /** @property {Object} - The checklist required for the preset */
  get checkList () {
    return this.preset?.checkList
  }

  /** @property {Object} - A list of all connector types */
  get connectorTypes () {
    return this.preset?.connectorTypes
  }

  /** @property {Object} - A message displayed by the tooltip */
  get tooltipHelpMessage () {
    return this.preset?.tooltipHelpMessage
  }

  /**
  * Creates a preset model from a JSON object
  * @param {Object} json - JSON representation of the preset
  * @returns {module:models/preset} A preset model
  */
  makePresetModel (json) {
    let preset = null

    try {
      preset = Preset.fromJSON(json)
    } catch (error) {
      console.error(`Failed to make a preset model from ${json}`)
    }
    return preset
  }

  /**
   * Sets the new preset
   * @param {Object} preset - The preset model
   */
  setPreset (preset) {
    this.preset = preset
  }
}

export default PresetStore
