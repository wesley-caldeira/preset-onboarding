import { makeObservable, observable, action } from 'mobx'
import { CycleEnum } from '@models/CycleEnum'

/**
 * @classdesc Stores all cycles
 * @memberof stores
 */
class CycleStore {
  /** @property {string} currentCycle - The current cycle of the app */
  currentCycle = CycleEnum.INITIALIZING

  /** @property {boolean} isConnecting - Flags true if the user is configuring an entry */
  get isConnecting () {
    return this.currentCycle === CycleEnum.CONNECTING
  }

  /** @property {boolean} isTesting - Flags true if the user is testing a device */
  get isTesting () {
    return this.currentCycle === CycleEnum.TESTING
  }

  /** @property {boolean} isChecking - Flags true if the user is configuring an entry */
  get isChecking () {
    return this.currentCycle === CycleEnum.CHECKING
  }

  /** @property {boolean} isStepsDisplayed - Flags true if the steps section should be displayed */
  get isStepsDisplayed () {
    return !this.isTesting && !this.isConnecting && !this.isChecking
  }

  /** @property {boolean} isConfigurationDisplayed - Flags true if the configuration section should be displayed */
  get isConfigurationDisplayed () {
    return !this.isTesting && this.isConnecting
  }

  /** @property {boolean} isTestingDisplayed - Flags true if the scene advice (testing) section should be displayed */
  get isTestingDisplayed () {
    return this.isTesting && !this.isConnecting
  }

  /** @property {boolean} isConnectorDisplayed - Flags true if the connector schema should be displayed */
  get isConnectorDisplayed () {
    return this.isTesting || this.isConnecting || this.isChecking
  }

  /** @property {boolean} isChecklistDisplayed - Flags true if the preset checklist should be displayed */
  get isChecklistDisplayed () {
    return this.isChecking
  }

  constructor () {
    makeObservable(this, {
      currentCycle: observable,
      setCurrentCycle: action
    })
  }

  /**
  * Sets the current cycle of the app
  * @param {string} value - The current cycle value
  */
  setCurrentCycle (value) {
    this.currentCycle = value
  }
}

export default CycleStore
