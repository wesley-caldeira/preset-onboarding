import React from 'react'
import { Feedback } from '@sat-mtl/ui-components'

const { Tooltip } = Feedback

/**
 * Renders a help tooltip for the preset
 * @selector `#PresetHelpTooltip`
 * @param {string} message - The message that the tooltip displays
 * @param {boolean} visible - Makes the tooltip visible if true
 * @param {node} children - The dom element to which the tooltip is attached
 * @param {boolean} visibleOnHover - Makes the tooltip visible on hover if true
 * @returns {external:react/Component} The connection schema content
 */
const PresetHelpTooltip = ({ message, visible, children, visibleOnHover }) => {
  return (
    <div id='PresetHelpTooltip'>
      <Tooltip
        side='top'
        content={(
          <div style={{ display: 'flex', alignItems: 'center' }}>
            {message}
          </div>
      )}
        visible={visible}
        visibleOnHover={visibleOnHover}
      >
        {children}
      </Tooltip>
    </div>
  )
}

export default PresetHelpTooltip
