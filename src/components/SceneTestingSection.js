import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import ProgressBar from '@utils/ProgressBar'
import { Common } from '@sat-mtl/ui-components'
import { AppStoresContext } from '@components/App'

import '@styles/SceneTestingSection.scss'
import { useTranslation } from 'react-i18next'

const { Button } = Common

/**
 * Renders the content of the tester container when a video is tested
 * @selector `#TesterMediaContainerImg`
 * @returns {external:react/Component} The content for the video test container
 */
const TesterMediaContainerImg = () => {
  return (
    <div id='TesterMediaContainerImg'>
      image goes here
    </div>
  )
}

/**
 * Renders the content of the scene testing section header
 * @selector `.TesterTitle`
 * @returns {external:react/Component} The title of the testing section header
 */
const SceneTesterHeader = () => {
  const { t } = useTranslation()

  return (
    <div className='TesterTitle'>
      <p>{t('Check that your device is working properly')}</p>
    </div>
  )
}

/**
 * Renders the details of the scene tester
 * @selector `.Tester`
 * @returns {external:react/Component} The details of the tester container
 */
const SceneTesterDetails = observer(() => {
  const { localSceneStore } = useContext(AppStoresContext)
  const { isTestingVideoEntry, isTestingAudioEntry } = localSceneStore
  const { t } = useTranslation()

  return (
    <div className='Tester'>
      <p className='TesterCaption'>{t(localSceneStore.currentPin.testerCaption)}</p>
      <div className='TesterTools'>
        <div className='TesterMediaContainer'>
          {isTestingVideoEntry && <TesterMediaContainerImg />}
          {isTestingAudioEntry && <div style={{ height: '2rem', width: '100%' }}><ProgressBar /></div>}
        </div>
        <div className='TesterButtonsContainer'>
          <Button size='normal' type='secondary' variant='outlined' onClick={() => console.log('testing')}>TEST</Button>
        </div>
      </div>
    </div>
  )
})

/**
 * Renders the scene testing section of the preset
 * @selector `#SceneTestingSection`
 * @returns {external:react/Component} The preset scene testing section
 */
const SceneTestingSection = () => {
  return (
    <section id='SceneTestingSection'>
      <SceneTesterHeader />
      <SceneTesterDetails />
    </section>
  )
}

export default SceneTestingSection
