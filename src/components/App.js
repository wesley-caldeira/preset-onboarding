import React, { createContext } from 'react'
import { Context, i18nConfig, Shared } from '@sat-mtl/ui-components'

import populateStores from '~/src/populateStores'
import demoPreset from '@assets/json/demoPreset'

import PageLayout from '@components/PageLayout'
import LocalesEn from '@assets/locales/en/locale.json'
import LocalesFr from '@assets/locales/fr/locale.json'
import NpmPackage from '~/package.json'

// @todo Debug ui-components css, it triggers the heap error
import '@sat-mtl/ui-components/ui-components.css'

import '@styles/App.scss'
import { useTranslation } from 'react-i18next'

const { ThemeProvider } = Context
const { VersionSection } = Shared

const stores = populateStores()

/**
 * @constant {external:react/Context} AppStoresContext - Dispatches all the App stores
 * @memberof components.App
 */
export const AppStoresContext = createContext({})

/**
 * @constant {string} repoLink - The link to the project's repository
 * @memberof components.App
 */
export const repoLink = 'https://gitlab.com/sat-mtl/telepresence/scenic-light/preset-onboarding'

/**
 * Provides the i18n configuration to the app
 * @param {json} LocalesEn - The json object for en translations
 * @parma {json} LocalesFr - The json object for fr translations
 * @param {?string} - The default language to use when a chosen language is not available
 * @param {?string} - The language param that is used to changed the language, if required
 * @returns {external:ui-components/src} - The configuration object for i18n
 */
i18nConfig(LocalesEn, LocalesFr, 'en', 'lang')

/**
  * Renders the App
*/
function App () {
  const { presetStore } = stores
  const { t } = useTranslation()

  // ---------- demo setup only
  const presetModel = presetStore.makePresetModel(demoPreset)
  presetStore.setPreset(presetModel)
  // ------------------------------------------

  const title = t('Software version')

  return (
    <ThemeProvider value='gabrielle'>
      <AppStoresContext.Provider value={stores}>
        <PageLayout />
        <VersionSection NpmPackage={NpmPackage} repoLink={repoLink} title={title} />
      </AppStoresContext.Provider>
    </ThemeProvider>
  )
}

export default App
