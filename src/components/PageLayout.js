import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { Context } from '@sat-mtl/ui-components'

import PresetSceneSchema from '@components/PresetSceneSchema'
import PresetHeader from '@components/PresetHeader'
import PresetFooter from '@components/PresetFooter'
import { PresetTitleEnum } from '@models/PresetEnum'

const { ThemeContext } = Context

/**
 * Renders the page layout of the preset
 * @selector `#PageLayout`
 * @returns {external:react/Component} The page layout for preset workflow
 */
const PageLayout = observer(() => {
  const theme = useContext(ThemeContext)

  return (
    <div id='PageLayout' className={`Page-${theme}`}>
      <PresetHeader title={PresetTitleEnum.SMALL_HALLS} />
      <main>
        <PresetSceneSchema />
      </main>
      <PresetFooter />
    </div>
  )
})

export default PageLayout
