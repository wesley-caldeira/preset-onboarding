import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { Common } from '@sat-mtl/ui-components'

import { AppStoresContext } from '@components/App'
import SceneSchemaPin from '@components/SceneSchemaPin'
import ConnectionModal from '@components/ConnectionModal'
import PresetHelpTooltip from '@components/PresetHelpTooltip'

import StaticPinGroup from '@assets/icon/staticPinGroup.svg'

import { CycleEnum } from '@models/CycleEnum'

import '@styles/PresetSceneSchema.scss'
import { useTranslation } from 'react-i18next'

const { Icon, Button } = Common

/**
 * Renders a pin in a scene schema
 * @param {object} pin - The pin object
 * @returns {external:react/Component} The Pin of a scene schema
 */
const Pin = ({ pin }) => {
  const { localSceneStore } = useContext(AppStoresContext)
  const pinId = pin.id
  const iconType = pin.iconType.type

  let $pin = ''

  $pin = (
    <SceneSchemaPin
      isActive={localSceneStore.isActivePin(pinId)}
      isChecked={localSceneStore.isCheckedPin(pinId)}
      color='#FDD153'
    >
      <Icon type={iconType} />
    </SceneSchemaPin>
  )

  return $pin
}

/**
 * Renders a dynamic scene pin; the device to be connected and tested
 * @selector `.DynamicPinContainer`
 * @param {object} pin - The pin object
 * @returns {external:react/Component} The scene dynamic pin
 */
const DynamicPinGroup = ({ pin }) => {
  const { cycleStore, localSceneStore } = useContext(AppStoresContext)
  const pinId = pin.id

  return (
    <div className='DynamicPinContainer' style={{ top: pin.position.top, right: pin.position.right }}>
      <div className='PinItems'>
        {localSceneStore.isCheckedPin(pinId) ? <div className='PinChecked'><Icon type='check' /></div> : null}
        <Button
          shape='circle'
          variant='text'
          onClick={() => {
            cycleStore.setCurrentCycle(CycleEnum.CONNECTING)
            localSceneStore.setActivePin(pin)
            localSceneStore.setCurrentPin(pin)
          }}
        >
          <Pin pin={pin} />
        </Button>
      </div>
    </div>
  )
}

/**
 * Renders the scene schema of the preset
 * @selector `#PresetSceneSchema`
 * @returns {external:mobx-react/ObserverComponent} The scene schema
 */
const PresetSceneSchema = observer(() => {
  const { localSceneStore, cycleStore, presetStore } = useContext(AppStoresContext)
  const { t } = useTranslation()

  return (
    <>
      <section id='PresetSceneSchema'>
        <p className='SceneSchemaTitle'>{t('Report the manual connection of the devices on the plan below')}</p>
        <div className='SceneSchemaContainer' style={{ width: '100%' }}>
          <PresetHelpTooltip
            visible={cycleStore.currentCycle === CycleEnum.INITIALIZING && localSceneStore.availablePins.length !== localSceneStore.checkedPins.size}
            message={t(presetStore.tooltipHelpMessage)}
            visibleOnHover={cycleStore.currentCycle === CycleEnum.INITIALIZING}
          >
            <StaticPinGroup className='StaticPinGroup' />
          </PresetHelpTooltip>
          {localSceneStore.availablePins.map(dynamicScene =>
            <DynamicPinGroup key={dynamicScene.id} pin={dynamicScene} />
          )}
        </div>
        <div className='SceneSchemaLocation'>
          <div className='LocationPin' />
          {t('Montreal')}
        </div>
        {(cycleStore.isConnecting || cycleStore.isTesting) && <ConnectionModal pin={localSceneStore.currentPin} />}
      </section>
    </>
  )
})

export default PresetSceneSchema
