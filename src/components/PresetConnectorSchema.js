import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { useTranslation } from 'react-i18next'

import { Common, Inputs } from '@sat-mtl/ui-components'
import { AppStoresContext } from '@components/App'
import { CycleEnum } from '@models/CycleEnum'

import '@styles/PresetConnectorSchema.scss'

const { Icon } = Common
const { Checkbox } = Inputs

/**
 * Renders the connector schema detailed instructions
 * @selector `.ConnectorSchemaInstructions`
 * @returns {external:react/Component} The connector schema instructions
 */
const ConnectorSchemaInstructions = () => {
  const { presetStore } = useContext(AppStoresContext)
  const { t } = useTranslation()
  return (
    <div className='ConnectorSchemaInstructions'>
      {t(presetStore.connectionInstructions)}
    </div>
  )
}

/**
 * Renders a connector panel checkbox
 * @param {object} connector - The connector object
 * @returns {external:mobx-react/ObserverComponent} The connector panel checkbox
 */
const ConnectorPanelCheckbox = observer(({ connector }) => {
  const { cycleStore, connectorStore, localSceneStore } = useContext(AppStoresContext)
  const { selectedConnectors } = connectorStore

  const pinId = localSceneStore.currentPin.id
  const isDisabled = connectorStore.isDisabledConnector(connector)

  return (
    <Checkbox
      size='normal'
      shape='circle'
      checked={Array.from(selectedConnectors.values()).includes(connector.id)}
      disabled={isDisabled}
      status={isDisabled ? 'disabled' : 'focus'}
      onChange={() => {
        cycleStore.setCurrentCycle(CycleEnum.TESTING)
        connectorStore.updateSelectedConnectors(pinId, connector)
        connectorStore.setCurrentConnectorId(connector.id)
      }}
    >
      <div style={{ width: '1rem', height: '0,75rem' }}>
        <Icon type='check' />
      </div>
    </Checkbox>
  )
})

/**
 * Renders the Connector panel
 * @selector `.ConnectorPanel`
 * @param {string} title - The title of the connector panel indicating the type of the connector
 * @param {Object} connectorsGroup - An array of the available connectors
 * @returns {external:react/Component} The connection schema content
 */
const ConnectorPanel = ({ title, connectorsGroup }) => {
  return (
    <div className='ConnectorPanel'>
      <div className='ConnectorPanelTitle'>{title}</div>
      <div className='ConnectorsContainer'>
        {connectorsGroup?.map((item, i) => {
          return (
            <div key={i} className='Connector'>
              <span>{i + 1}</span>
              <ConnectorPanelCheckbox connector={item} />
            </div>
          )
        })}
      </div>
    </div>
  )
}

/**
 * Renders the connector schema of the preset
 * @selector `#PresetConnectorSchema`
 * @returns {external:mobx-react/ObserverComponent} The preset connector schema
 */
const PresetConnectorSchema = observer(() => {
  const { connectorStore, presetStore } = useContext(AppStoresContext)
  const types = presetStore?.connectorTypes

  return (
    <section id='PresetConnectorSchema'>
      <ConnectorSchemaInstructions />
      <div className='ControlPanelContainer'>
        {types.map(type => <ConnectorPanel key={type} title={type} connectorsGroup={connectorStore.getConnectorsByType(type)} />)}
      </div>
    </section>
  )
})

export default PresetConnectorSchema
