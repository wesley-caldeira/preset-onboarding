import React, { useContext } from 'react'
import { observer } from 'mobx-react-lite'
import { Common } from '@sat-mtl/ui-components'
import { useTranslation } from 'react-i18next'

import { AppStoresContext } from '@components/App'
import PresetConnectorSchema from '@components/PresetConnectorSchema'
import SceneTestingSection from '@components/SceneTestingSection'
import SceneSchemaPin from '@components/SceneSchemaPin'

import { CycleEnum } from '@models/CycleEnum'

const { Icon, Button } = Common

/**
 * Renders the header of the connection modal
 * @selector `.ConnectionModalHeader`
 * @returns {external:react/Component} The connection modal header
 */
const ConnectionModalHeader = () => {
  const { localSceneStore, cycleStore, connectorStore } = useContext(AppStoresContext)
  const pin = localSceneStore.currentPin

  const iconType = pin.iconType.type
  const pinLabel = pin.label

  return (
    <div className='ConnectionModalHeader'>
      <div className='ConnectorDetails'>
        <SceneSchemaPin
          isChecked
          // @todo map to this color when user is self and other two colors for partners
          color='#FDD153'
        >
          <Icon type={iconType} />
        </SceneSchemaPin>
        <span>{pinLabel}</span>
      </div>

      <Button
        id='CloseModalButton'
        size='tiny'
        shape='circle'
        variant='text'
        onClick={() => {
          cycleStore.setCurrentCycle(CycleEnum.INITIALIZING)
          connectorStore.selectedConnectors.delete(localSceneStore.currentPin.id)
          localSceneStore.activePin = null
        }}
      >
        <Icon type='closeButton' />
      </Button>
    </div>
  )
}

/**
 * Renders the footer of the connection modal of the preset
 * @selector `.ConnectionModalFooter`
 * @returns {external:react/Component} The connection  modal footer
 */
const ConnectionModalFooter = () => {
  const { t } = useTranslation()
  const { cycleStore, localSceneStore, connectorStore } = useContext(AppStoresContext)

  return (
    <footer className='ConnectionModalFooter'>
      <Button size='normal' variant='text' onClick={() => { console.log('signaling') }}>{t('Signal')}</Button>
      <Button
        size='normal'
        variant='contained'
        onClick={() => {
          cycleStore.setCurrentCycle(CycleEnum.INITIALIZING)
          localSceneStore.setCheckedPin(localSceneStore.currentPin.id, localSceneStore.currentPin)
          connectorStore.setCurrentConnectorId('')
        }}
      >{t('Validate')}
      </Button>
    </footer>
  )
}

/**
 * Renders the connection modal of the preset
 * @selector `#ConnectionModal`
 * @returns {external:mobx-react/ObserverComponent} The connection modal
 */
const ConnectionModal = observer(() => {
  const { connectorStore } = useContext(AppStoresContext)

  return (
    <section id='ConnectionModal'>
      <ConnectionModalHeader />
      <PresetConnectorSchema />
      {connectorStore.canTestConnector && <SceneTestingSection />}
      {connectorStore.canTestConnector && <ConnectionModalFooter />}
    </section>
  )
})

export default ConnectionModal
