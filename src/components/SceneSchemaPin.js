import React from 'react'
import { createUseStyles } from 'react-jss'

import '@styles/SceneSchemaPin.scss'

/**
 * Hook used to set parametrized styles
 * @see {@link https://cssinjs.org JSS library}
 */
const useStyles = createUseStyles({
  connector: {
    borderStyle: 'solid',
    backgroundColor: props => props.color,
    opacity: props => props.isActive || props.isChecked ? 1 : 0.5,
    width: props => props.isActive || props.isChecked ? '2.1rem' : '2rem',
    height: props => props.isActive || props.isChecked ? '2.1rem' : '2rem',
    boxShadow: props => props.isActive ? `0px 0px 40px 0px ${props.color}` : ''
  },
  connectorWrapper: {
    padding: '.2rem',
    border: props => props.isActive ? `2px solid ${props.color}` : '',
    borderRadius: '100%',
    color: props => props.color
  }
})

/** Renders a scene schema pin
 * @param {Object} props - Props that are passed to the react component
 * @returns {external:react/Component} The scene schema pin
*/
function SceneSchemaPin (props) {
  const classes = useStyles(props)

  return (
    <div className={classes.connectorWrapper}>
      <div className={`${classes.connector} SceneSchemaPin`}>
        <span className='ConnectorContent'>
          {props.children}
        </span>
      </div>
    </div>
  )
}

export default SceneSchemaPin
