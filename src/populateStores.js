import PresetStore from '@stores/PresetStore'
import ConnectorStore from '@stores/ConnectorStore'
import CycleStore from '@stores/CycleStore'
import LocalSceneStore from '@stores/LocalSceneStore'

/**
 * Creates all the app's Stores
 * @returns {object} Object with all stores used by Scenic
 */
function populateStores () {
  // Preset Stores
  const cycleStore = new CycleStore()
  const presetStore = new PresetStore(cycleStore)
  const localSceneStore = new LocalSceneStore(presetStore, cycleStore)
  const connectorStore = new ConnectorStore(presetStore, cycleStore, localSceneStore)

  return {
    presetStore,
    cycleStore,
    localSceneStore,
    connectorStore
  }
}

export default populateStores
