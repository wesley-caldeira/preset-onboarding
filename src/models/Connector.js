
/**
 * @classdesc Model used to create a new connector model from a JSON object
 * @memberof models
 */
class Connector {
  /**
   * Instantiates a new connector model
   * @param {string} id - ID of the connector
   * @param {string} type - Type of the connector
   * @param {Object} media - An array of the media types of the connector
   * @param {Object} direction - An array of the possible directions of the connector
   */
  constructor (id, type, media, direction) {
    this.id = id
    this.type = type
    this.media = media
    this.direction = direction
  }

  /**
   * Parses a JSON element to a new connector model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let connector = null

    try {
      const { id, type, media, direction } = json
      connector = new Connector(id, type, media, direction)
    } catch (error) {
      console.error(`failed to load the connector because ${error.message}`)
    }

    return connector
  }

  /**
   * Parses multiple JSON elements to new connector models from a json array
   * @param {Object} jsonArray - A JSON array
   * @static
   */
  static fromArray (jsonArray) {
    return jsonArray.map(json => Connector.fromJSON(json))
  }
}

export default Connector
