
/**
 * @classdesc Model used to create a new scene model from a JSON object
 * @memberof models
 */
class LocalScene {
  /**
   * Instantiates a new pin group model
   * @param {string} id - ID of the pin group
   * @param {string} [label=''] - Label of the pin group
   * @param {Object} iconType - An array of icon objects
   * @param {Object} position - An object containing the absolute position information of a pin group
   * @param {Object} direction - Possible direction of the pin Group
   * @param {string} [transformation=''] - Possible transformation information of the pin group
   */
  constructor (id, label = '', iconType, position, media, testerCaption = '', direction = [], transformation = '') {
    this.id = id
    this.label = label
    this.iconType = iconType
    this.position = position
    this.media = media
    this.testerCaption = testerCaption
    this.direction = direction
    this.transformation = transformation
  }

  /**
   * Parses a JSON element to a new scene model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let scene = null

    try {
      const { id, label, iconType, position, media, testerCaption, direction, transformation } = json
      scene = new LocalScene(id, label, iconType, position, media, testerCaption, direction, transformation)
    } catch (error) {
    }
    return scene
  }

  /**
   * Parses multiple JSON elements to new scene models from a json array
   * @param {Object} jsonArray - A JSON array
   * @static
   */
  static fromArray (jsonArray) {
    return jsonArray.map(json => LocalScene.fromJSON(json))
  }
}

export default LocalScene
