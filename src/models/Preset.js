import Ajv from 'ajv'
import { v4 as uuidv4 } from 'uuid'

import itemSchema from '@models/schemas/item.schema.json'
import entrySchema from '@models/schemas/entry.schema.json'
import presetSchema from '@models/schemas/preset.schema.json'

/** @todo bypass the strict mode error keyword */
const presetValidator = new Ajv({
  schemas: [
    itemSchema,
    entrySchema,
    presetSchema
  ],
  strict: false
})

/**
 * @classdesc Model used to create a new preset model from a JSON object
 * @memberof models
 */
class Preset {
  /**
   * Instantiates a new preset model
   * @param {string} id - ID of the preset
   * @param {Object} deviceList - Array of the required devices
   * @param {Object} input - An object containing information of all input resources and the respective devices
   * @param {Object} output - An object containing information of all output resources and the respective devices
   * @param {Object} checkList - Array of the required items to check
   */
  constructor (id, checkList, deviceList, connectionInstructions, connectorTypes, tooltipHelpMessage) {
    this.id = id
    this.checkList = checkList
    this.deviceList = deviceList
    this.connectionInstructions = connectionInstructions
    this.connectorTypes = connectorTypes
    this.tooltipHelpMessage = tooltipHelpMessage
  }

  /**
   * Parses a JSON element to a new preset model
   * @param {Object} json - A JSON element
   * @static
   */
  static fromJSON (json) {
    let preset = null
    const isValid = presetValidator.validate(presetSchema, json)

    if (isValid) {
      const id = uuidv4()
      const { checkList, deviceList, connectionInstructions, connectorTypes, tooltipHelpMessage } = json
      preset = new Preset(id, checkList, deviceList, connectionInstructions, connectorTypes, tooltipHelpMessage)
    } else {
      throw new Error(presetValidator.errorsText())
    }
    return preset
  }
}

export default Preset
